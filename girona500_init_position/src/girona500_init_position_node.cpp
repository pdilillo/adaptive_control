#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <eigen3/Eigen/Core>
#include <cola2_msgs/NavSts.h>
#include <girona500_adaptive_control/DesiredPose.h>
#define UP 1
#define DOWN 2

using namespace Eigen;
using namespace std;

VectorXd pose_ini(6);
bool pose_init = false;

int state = UP;
bool first_time = true;

void trapezoidal(double qi, double qf, double tf, double t, double &q, double &dq, double &ddq)
{

  /*
        Generates a 1-dimensional trapezoidal velocity profile trajectory

        input:

                double qi		dim: 1		initial value
                double qf		dim: 1		final value
                double tf		dim: 1		final time of the trajectory
                double t		dim: 1		current time
                double &q		dim: 1		output target position
                double &dq		dim: 1		output target velocity
                double &ddq		dim: 1		output target acceleration

  */

  double dq_c = 2 * (qf - qi) / (tf * 1.3);

  double tc = (qi - qf + dq_c * tf) / dq_c;
  double ddq_c = pow(dq_c, 2) / (qi - qf + dq_c * tf);

  if (t >= 0 && t <= tc)
  {

    q = qi + 0.5 * ddq_c * pow(t, 2);
    dq = ddq_c * t;
    ddq = ddq_c;
  }

  else if (t > tc && t <= tf - tc)
  {

    q = qi + ddq_c * tc * (t - 0.5 * tc);
    dq = ddq_c * tc;
    ddq = 0;
  }

  else if (t > tf - tc && t <= tf)
  {

    q = qf - 0.5 * ddq_c * pow(tf - t, 2);
    dq = -ddq_c * t + ddq_c * tf;
    ddq = -ddq_c;
  }
  else if (t > tf)
  {
    q = qf;
    dq = 0;
    ddq = 0;
  }
  if (qi == qf)
  {

    q = qi;
    dq = 0;
    ddq = 0;
  }
}

void NavCB(const cola2_msgs::NavStsConstPtr &msg)
{

  if (!pose_init)
  {
    // Update pose feedback
    pose_ini(0) = msg->position.north;
    pose_ini(1) = msg->position.east;
    pose_ini(2) = msg->position.depth;
    pose_ini(3) = msg->orientation.roll;
    pose_ini(4) = msg->orientation.pitch;
    pose_ini(5) = msg->orientation.yaw;

    //  quat_feedback_ = rpy2quat(pose_feedback_.tail(3));
    pose_init = true;
  }
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "girona500_init_position");

  ros::NodeHandle n;

  ros::Subscriber nav_sub = n.subscribe<cola2_msgs::NavSts>("/girona500/navigator/navigation", 1, NavCB);
  ros::Publisher pose_pub = n.advertise<girona500_adaptive_control::DesiredPose>("/girona500/adp_controller/pose_reference", 1);
  ros::Publisher twist_pub = n.advertise<geometry_msgs::TwistStamped>("/girona500/adp_controller/twist_reference", 1);
  ros::Rate loop_rate(10);

  girona500_adaptive_control::DesiredPose PoseMsg;
  geometry_msgs::TwistStamped TwistMsg;

  Vector3d position, velocity, acceleration;
  Vector3d angposition, angvelocity, angacceleration;
  VectorXd pose_fin(6);

  double t = 0;
  double tf = 40;
  double ts = 0.1;
  ros::Time start, end;
  int count = 0, times = 6;

  while (ros::ok())
  {

    if (pose_init)
    {

      if (first_time)
      {
        start = ros::Time::now();
        first_time = false;
      }
      pose_fin << 0.0, 0.0, 2.0, 0.0, 0.0, 0.0;

      trapezoidal(pose_ini(0), pose_fin(0), tf, t, position(0), velocity(0), acceleration(0));
      trapezoidal(pose_ini(1), pose_fin(1), tf, t, position(1), velocity(1), acceleration(1));
      trapezoidal(pose_ini(2), pose_fin(2), tf, t, position(2), velocity(2), acceleration(2));

      trapezoidal(pose_ini(3), pose_fin(3), tf, t, angposition(0), angvelocity(0), angacceleration(0));
      trapezoidal(pose_ini(4), pose_fin(4), tf, t, angposition(1), angvelocity(1), angacceleration(1));
      trapezoidal(pose_ini(5), pose_fin(5), tf, t, angposition(2), angvelocity(2), angacceleration(2));

      t += ts;

      TwistMsg.twist.linear.x = velocity(0);
      TwistMsg.twist.linear.y = velocity(1);
      TwistMsg.twist.linear.z = velocity(2);
      TwistMsg.twist.angular.x = angvelocity(0);
      TwistMsg.twist.angular.y = angvelocity(1);
      TwistMsg.twist.angular.z = angvelocity(2);

      PoseMsg.position.x = position(0);
      PoseMsg.position.y = position(1);
      PoseMsg.position.z = position(2);
      PoseMsg.rpy.x = 0.0;
      PoseMsg.rpy.y = 0.0;
      PoseMsg.rpy.z = angposition(2);

      pose_pub.publish(PoseMsg);
      
    }
    ros::spinOnce();

    loop_rate.sleep();
  }

  return 0;
}
