#include <girona500_adaptive_control.h>

void OdometryCB(nav_msgs::Odometry){


}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "g500_adaptive_control");
  ros::NodeHandle n;
  ros::Publisher force_pub = n.advertise<cola2_msgs::BodyForceReq>("/girona500/controller/body_force_req", 1);
  ros::Publisher velocity_pub = n.advertise<cola2_msgs::BodyVelocityReq>("/girona500/controller/body_velocity_req", 1);
  ros::Subscriber odometry_sub = n.subscribe<nav_msgs::Odometry>("/girona500/navigator/odometry",1,OdometryCB);

  ros::Rate loop_rate(10);
  
  cola2_msgs::BodyForceReq ReqMsg;
  cola2_msgs::BodyVelocityReq VelocityReqMsg;
  cola2_msgs::GoalDescriptor GoalMsg;
  geometry_msgs::Wrench WrenchMsg;
  geometry_msgs::Twist TwistMsg;
  cola2_msgs::Bool6Axis BoolMsg;
  
  GoalMsg.requester = "prova";
  GoalMsg.priority = 10;
  
  WrenchMsg.force.x = 400;
  WrenchMsg.force.y = 0.0;
  WrenchMsg.force.z = 0.0;
  WrenchMsg.torque.x = 0.0;
  WrenchMsg.torque.y = 0.0;
  WrenchMsg.torque.z = 0.0;
  
  TwistMsg.linear.x = 0.4;
  TwistMsg.linear.y = 0;
  TwistMsg.linear.z = 0;
  TwistMsg.angular.x = 0;
  TwistMsg.angular.y = 0;
  TwistMsg.angular.z = 0; 
  
  BoolMsg.x = false;
  BoolMsg.y= false;
  BoolMsg.z = false;
  BoolMsg.roll = false;
  BoolMsg.pitch = false;
  BoolMsg.yaw = false;
  
  ReqMsg.header.frame_id = "/girona500/base_link";
  ReqMsg.goal =  GoalMsg;
  ReqMsg.wrench = WrenchMsg;
  ReqMsg.disable_axis = BoolMsg;
  
  VelocityReqMsg.header.frame_id = "/girona500/base_link";
  VelocityReqMsg.goal =  GoalMsg;
  VelocityReqMsg.twist = TwistMsg;
  VelocityReqMsg.disable_axis = BoolMsg;
  
  ctb::ADPGains gains;
  gains.Kl << 1,1,1,1,1,1;
  gains.Kg << 1,1,1,1,1,1,1,1,1;
  gains.Ks << 1,1,1,1,1,1;
  gains.gamma << 0,0,0,0,0,0,0,0,0;
  gains.upGB_m << 100,100,100;
  gains.lowGB_m << 0,0,0;
  gains.up_f << 1,0,0;
  gains.low_f << 10,10,10;
  gains.up_m << 10,10,10;
  gains.low_m << 10,10,10;
  
  
  double ts = 0.1;
  
  AdaptiveControlVehicle controller(gains, ts);
  
  while (ros::ok())
  {
  
    ReqMsg.header.stamp = ros::Time::now();
    VelocityReqMsg.header.stamp = ros::Time::now();
    force_pub.publish(ReqMsg);
  //  velocity_pub.publish(VelocityReqMsg);
    ros::spinOnce();

    loop_rate.sleep();
    
  }


  return 0;
}
