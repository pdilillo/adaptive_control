#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <eigen3/Eigen/Core>
#include <cola2_msgs/NavSts.h>
#include <girona500_adaptive_control/DesiredPose.h>
#define UP 1
#define DOWN 2

using namespace Eigen;
using namespace std;

VectorXd pose_ini(6);
bool pose_init = false;

int state = UP;
bool first_time = true;
VectorXd pose(6);

void trapezoidal(double qi, double qf, double tf, double t, double &q, double &dq, double &ddq)
{

    /*
        Generates a 1-dimensional trapezoidal velocity profile trajectory

        input:

                double qi		dim: 1		initial value
                double qf		dim: 1		final value
                double tf		dim: 1		final time of the trajectory
                double t		dim: 1		current time
                double &q		dim: 1		output target position
                double &dq		dim: 1		output target velocity
                double &ddq		dim: 1		output target acceleration



 */

    double dq_c = 2 * (qf - qi) / (tf * 1.3);

    double tc = (qi - qf + dq_c * tf) / dq_c;
    double ddq_c = pow(dq_c, 2) / (qi - qf + dq_c * tf);

    if (t >= 0 && t <= tc)
    {

        q = qi + 0.5 * ddq_c * pow(t, 2);
        dq = ddq_c * t;
        ddq = ddq_c;
    }

    else if (t > tc && t <= tf - tc)
    {

        q = qi + ddq_c * tc * (t - 0.5 * tc);
        dq = ddq_c * tc;
        ddq = 0;
    }

    else if (t > tf - tc && t <= tf)
    {

        q = qf - 0.5 * ddq_c * pow(tf - t, 2);
        dq = -ddq_c * t + ddq_c * tf;
        ddq = -ddq_c;
    }
    else if (t > tf)
    {
        q = qf;
        dq = 0;
        ddq = 0;
    }
    if (qi == qf)
    {

        q = qi;
        dq = 0;
        ddq = 0;
    }
}

void NavCB(const cola2_msgs::NavStsConstPtr &msg)
{

    pose(0) = msg->position.north;
    pose(1) = msg->position.east;
    pose(2) = msg->position.depth;
    pose(3) = msg->orientation.roll;
    pose(4) = msg->orientation.pitch;
    pose(5) = msg->orientation.yaw;

    if (!pose_init)
    {
        // Update pose feedback
        pose_ini(0) = msg->position.north;
        pose_ini(1) = msg->position.east;
        pose_ini(2) = msg->position.depth;
        pose_ini(3) = msg->orientation.roll;
        pose_ini(4) = msg->orientation.pitch;
        pose_ini(5) = msg->orientation.yaw;

        //  quat_feedback_ = rpy2quat(pose_feedback_.tail(3));
        pose_init = true;
    }
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "girona500_trajectory");

    ros::NodeHandle n;

    ros::Subscriber nav_sub = n.subscribe<cola2_msgs::NavSts>("/girona500/navigator/navigation", 1, NavCB);
    ros::Publisher pose_pub = n.advertise<girona500_adaptive_control::DesiredPose>("/girona500/adp_controller/pose_reference", 1);
    ros::Publisher twist_pub = n.advertise<geometry_msgs::TwistStamped>("/girona500/adp_controller/twist_reference", 1);
    ros::Rate loop_rate(10);

    girona500_adaptive_control::DesiredPose PoseMsg;
    geometry_msgs::TwistStamped TwistMsg;

    Vector3d position, velocity, acceleration;
    Vector3d angposition, angvelocity, angacceleration;

    VectorXd pos_in(6);
    VectorXd pos_media1(6);
    VectorXd pos_media2(6);
    VectorXd pos_media3(6);
    VectorXd pos_media4(6);
    VectorXd pos_media5(6);
    VectorXd pos_media6(6);
    VectorXd pos_media7(6);
    VectorXd pos_media8(6);
    VectorXd pos_media9(6);
    VectorXd pos_media10(6);
    VectorXd pos_media11(6);
    VectorXd pos_fin(6);

    double up = 0.20;
    double down = -0.2;

    double time_switch = 100;
    double tf_pol = 50;


    time_switch = 40;
    tf_pol = 30;

    double t = 0.0;
    double ts = 0.1;

    VectorXd posdes(6), veldes(6), accdes(6);
    ros::Time start, end;
    start = ros::Time::now();
    while (ros::ok())
    {

        if (pose_init)
        {
            pos_in << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, 0.0, 0.0;
            pos_media1 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, up, 0.0;
            pos_media2 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, 0.0, 0.0;
            pos_media3 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, down, 0.0;
            pos_media4 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, 0.0, 0.0;
            pos_media5 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, up, 0.0;
            pos_media6 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, 0.0, 0.0;
            pos_media7 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, down, 0.0;
            pos_media8 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, 0.0, 0.0;
            pos_media9 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, up, 0.0;
            pos_media10 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, 0.0, 0.0;
            pos_media11 << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, down, 0.0;
            pos_fin << pose_ini(0), pose_ini(1), pose_ini(2), 0.0, 0.0, 0.0;

            if (t < time_switch)
            {

                trapezoidal(pos_in(0), pos_media1(0), tf_pol, t, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_in(1), pos_media1(1), tf_pol, t, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_in(2), pos_media1(2), tf_pol, t, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_in(3), pos_media1(3), tf_pol, t, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_in(4), pos_media1(4), tf_pol, t, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_in(5), pos_media1(5), tf_pol, t, posdes(5), veldes(5), accdes(5));
           
            }

            else if (t >= time_switch && t < 2 * time_switch)
            {
                trapezoidal(pos_media1(0), pos_media2(0), tf_pol, t - time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media1(1), pos_media2(1), tf_pol, t - time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media1(2), pos_media2(2), tf_pol, t - time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media1(3), pos_media2(3), tf_pol, t - time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media1(4), pos_media2(4), tf_pol, t - time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media1(5), pos_media2(5), tf_pol, t - time_switch, posdes(5), veldes(5), accdes(5));
              
            }

            else if (t >= 2 * time_switch && t < 3 * time_switch)
            {
                trapezoidal(pos_media2(0), pos_media3(0), tf_pol, t - 2 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media2(1), pos_media3(1), tf_pol, t - 2 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media2(2), pos_media3(2), tf_pol, t - 2 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media2(3), pos_media3(3), tf_pol, t - 2 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media2(4), pos_media3(4), tf_pol, t - 2 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media2(5), pos_media3(5), tf_pol, t - 2 * time_switch, posdes(5), veldes(5), accdes(5));
                
            }

            else if (t >= 3 * time_switch && t < 4 * time_switch)
            {
                trapezoidal(pos_media3(0), pos_media4(0), tf_pol, t - 3 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media3(1), pos_media4(1), tf_pol, t - 3 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media3(2), pos_media4(2), tf_pol, t - 3 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media3(3), pos_media4(3), tf_pol, t - 3 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media3(4), pos_media4(4), tf_pol, t - 3 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media3(5), pos_media4(5), tf_pol, t - 3 * time_switch, posdes(5), veldes(5), accdes(5));
               
            }

            else if (t >= 4 * time_switch && t < 5 * time_switch)
            {
                trapezoidal(pos_media4(0), pos_media5(0), tf_pol, t - 4 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media4(1), pos_media5(1), tf_pol, t - 4 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media4(2), pos_media5(2), tf_pol, t - 4 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media4(3), pos_media5(3), tf_pol, t - 4 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media4(4), pos_media5(4), tf_pol, t - 4 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media4(5), pos_media5(5), tf_pol, t - 4 * time_switch, posdes(5), veldes(5), accdes(5));
                
            }

            else if (t >= 5 * time_switch && t < 6 * time_switch)
            {
                trapezoidal(pos_media5(0), pos_media6(0), tf_pol, t - 5 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media5(1), pos_media6(1), tf_pol, t - 5 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media5(2), pos_media6(2), tf_pol, t - 5 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media5(3), pos_media6(3), tf_pol, t - 5 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media5(4), pos_media6(4), tf_pol, t - 5 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media5(5), pos_media6(5), tf_pol, t - 5 * time_switch, posdes(5), veldes(5), accdes(5));
                
            }

            else if (t >= 6 * time_switch && t < 7 * time_switch)
            {
                trapezoidal(pos_media6(0), pos_media7(0), tf_pol, t - 6 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media6(1), pos_media7(1), tf_pol, t - 6 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media6(2), pos_media7(2), tf_pol, t - 6 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media6(3), pos_media7(3), tf_pol, t - 6 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media6(4), pos_media7(4), tf_pol, t - 6 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media6(5), pos_media7(5), tf_pol, t - 6 * time_switch, posdes(5), veldes(5), accdes(5));
                
            }

            else if (t >= 7 * time_switch && t < 8 * time_switch)
            {
                trapezoidal(pos_media7(0), pos_media8(0), tf_pol, t - 7 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media7(1), pos_media8(1), tf_pol, t - 7 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media7(2), pos_media8(2), tf_pol, t - 7 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media7(3), pos_media8(3), tf_pol, t - 7 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media7(4), pos_media8(4), tf_pol, t - 7 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media7(5), pos_media8(5), tf_pol, t - 7 * time_switch, posdes(5), veldes(5), accdes(5));
                
            }
            
            else if (t >= 8 * time_switch && t < 9 * time_switch)
            {
                trapezoidal(pos_media8(0), pos_media9(0), tf_pol, t - 8 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media8(1), pos_media9(1), tf_pol, t - 8 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media8(2), pos_media9(2), tf_pol, t - 8 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media8(3), pos_media9(3), tf_pol, t - 8 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media8(4), pos_media9(4), tf_pol, t - 8 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media8(5), pos_media9(5), tf_pol, t - 8 * time_switch, posdes(5), veldes(5), accdes(5));
                
            }

            else if (t >= 9 * time_switch && t < 10 * time_switch)
            {
                trapezoidal(pos_media9(0), pos_media10(0), tf_pol, t - 9 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media9(1), pos_media10(1), tf_pol, t - 9 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media9(2), pos_media10(2), tf_pol, t - 9 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media9(3), pos_media10(3), tf_pol, t - 9 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media9(4), pos_media10(4), tf_pol, t - 9 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media9(5), pos_media10(5), tf_pol, t - 9 * time_switch, posdes(5), veldes(5), accdes(5));
                
            }

            else if (t >= 10 * time_switch && t < 11 * time_switch)
            {
                trapezoidal(pos_media10(0), pos_media11(0), tf_pol, t - 10 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media10(1), pos_media11(1), tf_pol, t - 10 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media10(2), pos_media11(2), tf_pol, t - 10 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media10(3), pos_media11(3), tf_pol, t - 10 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media10(4), pos_media11(4), tf_pol, t - 10 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media10(5), pos_media11(5), tf_pol, t - 10 * time_switch, posdes(5), veldes(5), accdes(5));
               
            }

            else if (t >= 11 * time_switch && t < 12 * time_switch)
            {
                trapezoidal(pos_media11(0), pos_fin(0), tf_pol, t - 11 * time_switch, posdes(0), veldes(0), accdes(0));
                trapezoidal(pos_media11(1), pos_fin(1), tf_pol, t - 11 * time_switch, posdes(1), veldes(1), accdes(1));
                trapezoidal(pos_media11(2), pos_fin(2), tf_pol, t - 11 * time_switch, posdes(2), veldes(2), accdes(2));
                trapezoidal(pos_media11(3), pos_fin(3), tf_pol, t - 11 * time_switch, posdes(3), veldes(3), accdes(3));
                trapezoidal(pos_media11(4), pos_fin(4), tf_pol, t - 11 * time_switch, posdes(4), veldes(4), accdes(4));
                trapezoidal(pos_media11(5), pos_fin(5), tf_pol, t - 11 * time_switch, posdes(5), veldes(5), accdes(5));
                
            }
            else{
                end = ros::Time::now();
                cout << "Elapsed time: " << end-start << "\n\n";
                system("rosnode kill rosbag_rec");  
                exit(0);
            }

            t += ts;

            PoseMsg.header.stamp = ros::Time::now();
            PoseMsg.position.x = posdes(0);
            PoseMsg.position.y = posdes(1);
            PoseMsg.position.z = posdes(2);
            PoseMsg.rpy.x = posdes(3);
            PoseMsg.rpy.y = posdes(4);
            PoseMsg.rpy.z = posdes(5);
            pose_pub.publish(PoseMsg);
/*
            TwistMsg.twist.linear.x = veldes(0);
            TwistMsg.twist.linear.y = veldes(1);
            TwistMsg.twist.linear.z = veldes(2);
            TwistMsg.twist.angular.x = veldes(3);
            TwistMsg.twist.angular.y = veldes(4);
            TwistMsg.twist.angular.z = veldes(5);
            twist_pub.publish(TwistMsg);
            */
        }
        ros::spinOnce();

        loop_rate.sleep();
    }

    return 0;
}
