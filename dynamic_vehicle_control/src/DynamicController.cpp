#include "dynamic_vehicle_control/DynamicController.h"

DynamicController::DynamicController(double ts)
{
	ts_ = ts;
}

DynamicController::~DynamicController()
{
}

void DynamicController::ReadFeedback(const Eigen::VectorXd &fbkVel)
{

	fbkVel_ = fbkVel;
}

void DynamicController::ReadPoseFeedback(const Eigen::VectorXd &fbkPose)
{

	fbkPose_ = fbkPose;
}

void DynamicController::SetDesiredVel(const Eigen::VectorXd &desVel)
{
	desVel_ = desVel;
}

void DynamicController::SetDesiredPose(const Eigen::VectorXd &desPose)
{
	desPose_ = desPose;
}

const Eigen::VectorXd &DynamicController::GetDynamicControl() const
{
	return dynamicControl_;
}

void DynamicController::GetVehicleOri(const Eigen::VectorXd &fbkQuat)
{
	fbkQuat_ = fbkQuat;
}

void DynamicController::ComputeDynamicControl()
{
}
