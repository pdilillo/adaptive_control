#include <libconfig.h++>
#include <iostream>
#include <rml/RML.h>
#include "test/DynamicControlFunctions.h"
#include <dynamic_control/PIDVehicleControl.h>
#include <dynamic_control/AdaptiveControlVehicle.h>
#include "test/DynamicControlDefines.h"

int main()
{
	const std::string rootFolder = "";
	const std::string confPathPID = rootFolder + "../config/PID.conf";
	const std::string confPathGoals = rootFolder + "../config/Goals.conf";
	const std::string confPathKinematic = rootFolder + "../config/KinematicControl.conf";
	std::vector<double> saturation;
	std::vector<ctb::PIDGains> pidGains;
	Eigen::VectorXd goal;
	bool usePID = true; //TODO read from file which control use.
	bool controlVelocity = false;
	bool controlPosition = true;
	Eigen::VectorXd gamma;
	gamma.setOnes(9);
	gamma = 0.01 * gamma;

	double attitudeGain, positionGain, positionThreshold, attitudeThreshold;
	if (controlPosition)
	{
		if (!ConfigureKinematicControl(confPathKinematic, &attitudeGain, &positionGain, &attitudeThreshold,
				&positionThreshold))
		{

			return 0;
		}
	}
	if (GetGainsAndSaturation(confPathPID, &pidGains, &saturation))
	{

		auto vehicleModel = std::make_shared < rml::VehicleModel > (rml::VehicleModel());
		vehicleModel->SetJacobian(Eigen::Matrix6d::Identity());
		PIDVehicleControl pidVehicleControl(pidGains, saturation, 0.1, vehicleModel);
		AdaptiveControlVehicle adaptiveControl(gamma, 0.1);
		Eigen::VectorXd desiredVel;
		if (GetVectorEigen(confPathGoals, goal, robust::goals::goalProperty))
		{
			if (controlVelocity)
			{
				desiredVel = goal;
				pidVehicleControl.SetDesiredVel(goal);
			} else if (controlPosition)
			{
				desiredVel = ComputeVelocity(goal, vehicleModel->GetPositionOnInertial(), attitudeGain, positionGain,
						attitudeThreshold, positionThreshold);

			}
			pidVehicleControl.SetDesiredVel(desiredVel);
			adaptiveControl.SetDesiredVel(desiredVel);
			pidVehicleControl.ReadFeedback(vehicleModel->GetVelocityOnVehicle());
			adaptiveControl.ReadFeedback(vehicleModel->GetVelocityOnVehicle());
			std::cout << "computing control" << std::endl;
			Eigen::Quaternionf q;
			Eigen::Vector3d rpy_position=vehicleModel->GetPositionOnInertial().GetFirstVect3();
			q = Eigen::AngleAxisf(rpy_position(0), Eigen::Vector3f::UnitX()) * Eigen::AngleAxisf(rpy_position(1), Eigen::Vector3f::UnitY())
					* Eigen::AngleAxisf(rpy_position(2), Eigen::Vector3f::UnitZ());
			Eigen::VectorXd quat(4);
			quat(0)=q.w();
			quat(1)=q.x();
			quat(2)=q.y();
			quat(3)=q.z();

			adaptiveControl.GetVehicleOri(quat);
			pidVehicleControl.ComputeDynamicControl();
			adaptiveControl.ComputeDynamicControl();
			std::cout << "adaptive \n" << adaptiveControl.GetDynamicControl() << std::endl;
			std::cout << "PID \n" << pidVehicleControl.GetDynamicControl() << std::endl;


		}

		else
		{
			std::cerr << "Unable to find find goal" << std::endl;
			return 0;

		}

	} else
	{
		std::cerr << "Unable to find find either gains and or saturation" << std::endl;
		return 0;
	}

}
