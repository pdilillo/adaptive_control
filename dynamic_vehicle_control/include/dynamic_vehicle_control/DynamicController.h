#ifndef __DYNAMICCONTROLLER_H__
#define __DYNAMICCONTROLLER_H__

#include <eigen3/Eigen/Dense>

/**
 * @brief DynamicController Class.
 * Abstract class providing an interface for implementing the dynamic control.
 * @note The deriving classes must implement the pure virtual method "ComputeDynamicControl()".
 */
class DynamicController
{
  public:
	/**
	 * @brief Constructor.
	 * @param[in] sampleTime: sample time of the control loop.
	 */
	DynamicController(double ts);
	/**
	 * @brief Default de-constructor.
	 */
	virtual ~DynamicController();
	/**
	 * @brief Method to read the velocity feedback.
	 * @param[in] fbkVel: velocity feedback.
	 */
	void ReadFeedback(const Eigen::VectorXd &fbkVel);
	void ReadPoseFeedback(const Eigen::VectorXd &fbkPose);

	/**
	 * @brief Method setting the desired velocity.
	 * @param[in] desVel: desiredVelocity.
	 */
	void SetDesiredVel(const Eigen::VectorXd &desVel);
	void SetDesiredPose(const Eigen::VectorXd &desPose);
	/**
	 * @brief Pure virtual method to compute the dynamic control.
	 * @note This method must be implemented in the derived classes.
	 */
	virtual void ComputeDynamicControl();
	/**
	 * @brief Method returning the computed control.
	 * @return Computed Control.
	 */
	const Eigen::VectorXd &GetDynamicControl() const;
	/**
	 * @brief Method returning the vehicle orientation in quaternion.
	 * @param fbkQuat vehicle attitude in quaternion.
	 */
	void GetVehicleOri(const Eigen::VectorXd &fbkQuat);

  protected:
	Eigen::VectorXd desVel_;		 //!< The vehicle desired velocity.
	Eigen::VectorXd desPose_;		 //!< The vehicle desired pose.
	Eigen::VectorXd fbkVel_;		 //!< The vehicle feedback velocity.
	Eigen::VectorXd fbkPose_;		 //!< The vehicle feedback pose.
	Eigen::VectorXd fbkQuat_;		 //!< The vehicle feedback attitude
	Eigen::VectorXd dynamicControl_; //!< The dynamic control.
	double ts_;						 //!< The sample time.
};

#endif
