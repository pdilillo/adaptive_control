#ifndef __ADAPTIVECONTROLVEHICLE_H__
#define __ADAPTIVECONTROLVEHICLE_H__

#include "DynamicController.h"
#include <eigen3/Eigen/Dense>
#include <dynamic_vehicle_control/DataStructs.h>

class AdaptiveControlVehicle : public DynamicController
{
  private:
	ctb::ADPGains adpGains_;
	ctb::PIDGains pidGains_;
	Eigen::VectorXd gamma_;
	Eigen::VectorXd gamma_dot_;
	Eigen::MatrixXd Ks_;
	Eigen::MatrixXd Kl_;
	Eigen::MatrixXd Kg_;

	Eigen::VectorXd s_;
	Eigen::MatrixXd R_;
	Eigen::MatrixXd Y_;

	Eigen::VectorXd z_;
	Eigen::VectorXd pose_error_;
	Eigen::VectorXd velocity_error_;

	Eigen::VectorXd upB_;
	Eigen::VectorXd lowB_;
	Eigen::VectorXd integrale;

	int type_; // 1: only velocity	2: pose+veocity

  public:
	AdaptiveControlVehicle(ctb::ADPGains adpGains, double sampleTime);
	AdaptiveControlVehicle(ctb::PIDGains pidGains, double sampleTime);
	~AdaptiveControlVehicle();
	void ComputeDynamicControl();
	Eigen::Matrix3d mySkew(Eigen::VectorXd v);
	Eigen::Matrix3d myQuat2Rot(Eigen::VectorXd e);
	void IntegrationSat(Eigen::VectorXd lowerB, Eigen::VectorXd upperB);
	void SetGains(ctb::ADPGains adpGains);
	ctb::ADPGains GetGains();
	Eigen::VectorXd GetPoseError();
	Eigen::VectorXd GetVelocityError();
	Eigen::VectorXd GetIntegralAction();
	Eigen::VectorXd GetIntegralActionAdaptive();
	void regressor();

	void SetControlType(int type);
	Eigen::VectorXd GetGamma();

	double ActivationFunction(double in, double thresh1, double thresh2);
};

#endif
