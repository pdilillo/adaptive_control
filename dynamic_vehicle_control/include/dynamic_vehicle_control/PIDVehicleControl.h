#ifndef __PIDVEHICLECONTROL_H__
#define __PIDVEHICLECONTROL_H__

#include <ctrl_toolbox/DigitalPID.h>
#include "DynamicController.h"
#include <iostream>
#include <vector>
#include <rml/RML.h>

class PIDVehicleControl: public DynamicController
{
public:
	/**
	 * @brief Class Constructur
	 * @param pidGains ctb::PIDGains struct containing Kp (Proportional gain), Ki (Integral gain),Kd (Derivative gain), Kff (Feed-forward gain
	 * N (Maximum gain for derivative part) and Tr (Tracking time constant for anti-windup)
	 * @param pidSaturation saturation value for PID controllers.
	 * @param ts sample time.
	 * @param vehicle the rml::VehicleModel.
	 */
    PIDVehicleControl(std::vector<ctb::PIDGains> pidGains, std::vector<double> pidSaturation, double ts);
	/**
	 *@brief Default de constructor.
	 */
	~PIDVehicleControl();
	/**
	 * @brief Method to compute the dynamic control.
	 * @note This method must be implemented in the derived classes.
	 */
	void ComputeDynamicControl() override;

    /**
     * @brief Method to update the controller parameters
     * @param pidControllers struct with the pid gains
     * @param pidSaturation vector containing the saturation values
     * @param ts sample time
     */
    void SetParameters (std::vector<ctb::PIDGains> pidControllers, std::vector<double> saturation);

    void Reset();
    /**
     * @brief Method returning the pid gains
     * @return  std::vector of the pid gains
     */
    std::vector<ctb::PIDGains> GetGains();
    /**
     * @brief Method returning the saturation value
     * @return std::vector of saturation values
     */
    std::vector<double> GetSaturation();
private:
	std::vector<ctb::DigitalPID> pidControllers_; //!< The PID controllers, one for each degree of freedom .
    std::vector<ctb::PIDGains> pidGains_;
    std::vector<double> saturations_;

};
#endif
