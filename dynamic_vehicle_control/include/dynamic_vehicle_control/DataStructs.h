#include <vector>

namespace ctb {

struct PIDGains {
    Eigen::VectorXd Kp = {}; //!< Proportional gain
    Eigen::VectorXd Ki = {}; //!< Integral gain
    Eigen::VectorXd Kd = {}; //!< Derivative gain
    Eigen::VectorXd Kff = {}; //!< Feed-forward gain

    PIDGains() = default;
};

struct LatLong {
    double latitude, longitude;
    LatLong()
        : latitude(0.0)
        , longitude(0.0)
    {
    }
};


struct ADPGains {
    Eigen::VectorXd Kl = {}; //!< Pose error Proportional gain
    Eigen::VectorXd Kg = {}; //!< Dynamic parameters update law gain
    Eigen::VectorXd Ks = {}; //!< Velocity error Proportional gain 
    Eigen::VectorXd gamma = {};
    Eigen::Vector3d upGB_m = {};
    Eigen::Vector3d lowGB_m = {};
    Eigen::Vector3d up_f = {};
    Eigen::Vector3d low_f = {};
    Eigen::Vector3d up_m = {};
    Eigen::Vector3d low_m = {};
    std::vector<bool> en_integration = {};
    
			
    //double Kff = { 0.0 }; //!< Feed-forward gain
    //double N = { 0.0 }; //!< Maximum gain for derivative part
    //double Tr = { 0.0 }; //!< Tracking time constant for anti-windup

    ADPGains() = default;
};


}

